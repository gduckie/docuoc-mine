import math
import numpy as np
import time
from PIL import Image


t1 = time.perf_counter()
# image  = Image.open("deskew_img/20.png")
image  = Image.open("output.png")

rotated = image.rotate(180)
save = rotated.save("output.jpg")
t2 = time.perf_counter()
print(f'Finished in {t2-t1} seconds')
