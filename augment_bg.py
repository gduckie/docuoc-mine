import subprocess
import argparse
import os
from PIL import Image
from random import randint
from var import image_valid
import concurrent.futures
import time

bg = [{"source": './bg/black.jpg', "name": 'black_bg'}, {"source": './bg/red.jpg', "name": 'red_bg'}, {"source": './bg/white.jpg', "name": 'white_bg'}, {"source": './bg/colorful.jpg', "name": 'colorful_bg'}]

processed = 0


def len_bg():
    return len(bg)

def log_info(processed, total):
    print("json{" + "\"processed\": \"{}\", \"total\": \"{}\"".format(processed, total) +"}")

def convertToCoor(coor, filename):
    x, y, w, h = coor
    if filename.split(".")[-1] in image_valid:
        img = Image.open(filename)
        # print('./' + source_folder + '/' +  filename)
        img_w, img_h = img.size
        xmin = x*img_w - (w*img_w)/2
        ymin = y*img_h - (h*img_h)/2
        xmax = w*img_w + xmin
        ymax = h*img_h + ymin
        return round(xmin), round(ymin), round(xmax), round(ymax)

def convertToYolo(coor, filename):
    img = Image.open(filename)
    img_w, img_h = img.size
    xmin, ymin, xmax, ymax = coor
    x_center = float((xmin + xmax)) / 2 / img_w
    y_center = float((ymin + ymax)) / 2 / img_h
    w = float((xmax - xmin)) / img_w
    h = float((ymax - ymin)) / img_h
    return round(x_center, 6), round(y_center, 6), round(w, 6), round(h, 6)

def shift_coor(yolo_coor, coor_after_bg, ori_img_path, new_img):
    # Get label coor from yolo coor in file txt
    xmin_field, ymin_field, xmax_field, ymax_field = convertToCoor(yolo_coor, ori_img_path)
    # coordinate of image when add above background
    xmin_img_bg, ymin_img_bg, _, _ = coor_after_bg

    xmin_rs = xmin_field + xmin_img_bg
    ymin_rs = ymin_field + ymin_img_bg
    xmax_rs = xmax_field + xmin_img_bg
    ymax_rs = ymax_field + ymin_img_bg
    yolo_converted = convertToYolo((xmin_rs, ymin_rs, xmax_rs, ymax_rs), new_img)
    return yolo_converted

def add_bg(filename, source_folder, dest_folder):
    for background in bg:
        bg_source = background['source']
        bg_name = background['name']
        background = Image.open(bg_source).convert('RGB')
        bg_w, bg_h = background.size
        if filename.split(".")[-1] in image_valid:
            img = Image.open("{}/{}".format(source_folder, filename))
            # print('./' + source_folder + '/' +  filename)
            img_w, img_h = img.size
            if not os.path.isfile(source_folder + '/' +  filename):
                return
            if img_w > bg_w or img_h > bg_h:
                # print('resize ' + source_folder + '/' +  filename)
                newsize = (1500, 1000)
                img = img.resize(newsize)
                img.save(source_folder + '/' +  filename)
                img_w = 1500
                img_h = 1000

            random_offset_x = randint(0, bg_w - img_w) # bg_w - img_w: To make sure image does not be overflow on background
            random_offset_y = randint(0, bg_h - img_h) # bg_h - img_h: To make sure image does not be overflow on background


            offset = (random_offset_x, random_offset_y)
            # Overlay image on background with random offset
            
            background.paste(img, offset)
            
            background.save("{}/{}_{}.{}".format(dest_folder, bg_name, os.path.splitext(filename)[0], "jpg"))


        

            # calculate xmin, ymin, xmax, ymax
            xmin = random_offset_x
            ymin = random_offset_y
            xmax = random_offset_x + img_w
            ymax = random_offset_y + img_h

            # calculate yolo format
            x_center = float((xmin + xmax)) / 2 / bg_w
            y_center = float((ymin + ymax)) / 2 / bg_h
            w = float((xmax - xmin)) / bg_w
            h = float((ymax - ymin)) / bg_h

            
            # 1. read coor from txt
            read_labeled_file = open("{}/{}.txt".format(source_folder, filename.split(".")[0]), "r")
            lines_lableled = read_labeled_file.readlines()

            # 2. edit shift coor and generate new txt file
            with open("{}/{}_{}.{}".format(dest_folder, bg_name, os.path.splitext(filename)[0], 'txt'), 'w') as f:
                for labeled in lines_lableled:
                    labeled_data = labeled.split(' ')
                    label_key, x, y, w, h = labeled_data
                    x_rs, y_rs, w_rs, h_rs = shift_coor((float(x), float(y), float(w), float(h)), (xmin, ymin, xmax, ymax), "{}/{}".format(source_folder, filename), "{}/{}_{}.{}".format(dest_folder, bg_name, os.path.splitext(filename)[0], "jpg"))


                    f.write("%d %.6f %.6f %.6f %.6f\n" % (int(label_key), x_rs, y_rs, w_rs, h_rs))


            background.close()
            img.close()

def helper_add_bg(para):
    return add_bg(para[0], para[1], para[2])

def execute(dest_folder, source_folder, para, total):
    global processed
    with concurrent.futures.ProcessPoolExecutor() as executor:
        for log in executor.map(helper_add_bg, para):
            processed += len(bg)
            print("\n==========================================================\n")
            log_info(processed, total)
            print("\n==========================================================\n")

def execute_main(source, destination):
    t1 = time.perf_counter()
    img_len = len([name for name in os.listdir(source) if os.path.isfile(os.path.join(source, name)) and name.split(".")[-1] in image_valid])
    txt_len = len([name for name in os.listdir(source) if os.path.isfile(os.path.join(source, name)) and name.endswith('txt')])

    if(img_len != txt_len):
        print("number of images and text files are invalid. Please make it equal!")
        return
    para = [(filename, source, destination) for _, filename in enumerate(os.listdir(source)) if os.path.isfile(os.path.join(source, filename)) and filename.split(".")[-1] in image_valid ]
    total = str(img_len * len(bg))
    execute(destination, source, para, total)
    # add_bg('bg/white.png', 'white', 'gsc_1.jpg', 'source', 'dest')
    t2 = time.perf_counter()
    print(f'Finished in {t2-t1} seconds')

# if __name__ == '__main__':

#     # Calling main() function
#     main()
