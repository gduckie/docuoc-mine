from PIL import Image
from var import image_valid

def convertToCoor(coor, filename):
    x, y, w, h = coor
    if filename.split(".")[-1] in image_valid:
        img = Image.open(filename)
        # print('./' + source_folder + '/' +  filename)
        img_w, img_h = img.size
        xmin = x*img_w - (w*img_w)/2
        ymin = y*img_h - (h*img_h)/2
        xmax = w*img_w + xmin
        ymax = h*img_h + ymin
        return round(xmin), round(ymin), round(xmax), round(ymax)

def convertToYolo(coor, filename):
    print(coor)
    img = Image.open(filename)
    img_w, img_h = img.size
    xmin, ymin, xmax, ymax = coor
    x_center = float((xmin + xmax)) / 2 / img_w
    y_center = float((ymin + ymax)) / 2 / img_h
    w = float((xmax - xmin)) / img_w
    h = float((ymax - ymin)) / img_h
    return round(x_center, 6), round(y_center, 6), round(w, 6), round(h, 6)

# yolo_coor: coor from txt file
# coor_after_bg: coor when add image on bg
# ori_img_path: path to original image
# new_img: path to new image (added bg)
def shift_coor(yolo_coor, coor_after_bg, ori_img_path, new_img):
    # Get label coor from yolo coor in file txt
    xmin_field, ymin_field, xmax_field, ymax_field = convertToCoor(yolo_coor, ori_img_path)
    # coordinate of image when add above background
    xmin_img_bg, ymin_img_bg, _, _ = coor_after_bg

    xmin_rs = xmin_field + xmin_img_bg
    ymin_rs = ymin_field + ymin_img_bg
    xmax_rs = xmax_field + xmin_img_bg
    ymax_rs = ymax_field + ymin_img_bg
    yolo_converted = convertToYolo((xmin_rs, ymin_rs, xmax_rs, ymax_rs), new_img)
    return yolo_converted

x = 0.589286
y = 0.646250
w = 0.601429
h = 0.620000
   
coor_image = (1010, 807, 3115, 2295)

# coor = convertToCoor((x,y,w,h), 'white_gsc_1.png')
# yolo = convertToYolo((1448, 1029, 2474, 1182), 'white_gsc_1.png')
shift_coor_rs = shift_coor((0.186698, 0.318884, 0.124466, 0.045027), (1010, 807, 3115, 2295), 'gsc_1.jpg', 'white_gsc_1.png')
print(shift_coor_rs)