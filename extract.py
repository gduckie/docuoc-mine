import subprocess
import argparse
import os
import json
from PIL import Image
from var import image_valid
from helper import yolobbox2bbox
from helper import get_info
from helper import validate_classes
from de import deskew
from de import rotate_by_degree_save
import cv2
import time

ap = argparse.ArgumentParser()
# path to test folder
ap.add_argument("-s", "--source", required=True,
    help="path to image file")
ap.add_argument("-d", "--destination", required=True,
    help="path to destination of result")
ap.add_argument("-c", "--config", required=False,
    help="path to config file (optional). If not set, default is 'cfg/yolov4-obj.cfg'")
ap.add_argument("-dt", "--data", required=False,
    help="path to data file (optional). If not set, default is 'data/obj.data'")
ap.add_argument("-w", "--weight", required=False,
    help="path to weight file (optional). If not set, default is 'yolov4-obj_last.weights'")
ap.add_argument("-e", "--extend", required=False,
    help="list value for extending detected boxes. Type: xmin, xmax, ymin, ymax, Default: 0, 0, 0, 0'")

args = vars(ap.parse_args())
#
#
source = args["source"]
config_file = args["config"] if args["config"] is not None else "cfg/yolov4-obj.cfg"
weight_file = args["weight"] if args["weight"] is not None else "yolov4-obj_last.weights"
data_file = args["data"] if args["data"] is not None else "data/obj.data"
dest = args["destination"]
extend = args["extend"] if args["extend"] is not None else '0,0,0,0'
# validate obj.names file
def run_shell(cmd):
    t = subprocess.run(cmd, capture_output=True, text=True, shell=True).stdout.strip("\n")
    return t.split("\n")

def extract_result(img_path):

    rotated_retry = [90, 180, -90, -180]
    result_extracted = []
    retry = 0
    # deskew process
    detect = '''darknet detector test ''' + data_file + ' ' + config_file + ' '  + weight_file + ''' -i 0 -thresh 0.6 ''' +  img_path + ''' -dont_show'''
    print('Run: ' + detect)

    result_detected = run_shell(detect)


    # print(result_detected)
    result_extracted = [rs.replace('result','') for rs in result_detected if 'result' in rs]
    if len(result_extracted) < 3:
        img_deskew = deskew(img_path)
        cv2.imwrite(img_path, img_deskew)

    while(len(result_extracted) < 3 and retry < 4): # result_extracted has no enough data
        detect_rotated = '''darknet detector test ''' + data_file + ' ' + config_file + ' '  + weight_file + ''' -i 0 -thresh 0.6 ''' + img_path  + ''' -dont_show'''
        result_rotated_detected = run_shell(detect_rotated)
        result_extracted = [rs.replace('result','') for rs in result_rotated_detected if 'result' in rs]
        print('{} with retry: {}'.format(result_extracted, retry))
        if len(result_extracted) < 3:
            print("retry with {} degree".format(str(rotated_retry[retry])))
            rotated_img = rotate_by_degree_save(img_path, rotated_retry[retry])
            # rotate
            retry+=1
        else:
            break

    # if result_extracted has no enough data -> go to deskew

    #
    print("json{" + "\"extracted\": {}".format(str(result_extracted).replace("'", ""))  + "}json")
    is_retry = retry > 0
    return result_extracted, is_retry

# cropped = image.crop(coor)
# cropped.save("{}/{}.png".format(dest, text), 'PNG')

def run():
    if not os.path.isdir(dest):
        print("destination folder is not existed")
        return
    if not validate_classes('data/obj.names', '', ''):
        print("Error in validating classes, please check again!")
        return

    t1 = time.perf_counter()
    results, is_retry = extract_result(source)

    image = Image.open(source)

    keyword_found = []
    for result in results:
        detected = json.loads(result)
        x = int(detected['coor']['x'])
        y = int(detected['coor']['y'])
        w = int(detected['coor']['w'])
        h = int(detected['coor']['h'])
        name = str(detected['class']).replace('/', '')
        cropped = image.crop(yolobbox2bbox(x, y, w, h, extend))
        cropped.save("{}/{}.png".format(dest, name), 'PNG')
        keyword_found.append(name)

    print(f'FOUND: {len(keyword_found)} keywords')
    print("json{" + "\"keyword_found\": \"{}\"".format(keyword_found)  + "}json")
    t2 = time.perf_counter()
    print(f'Finished in {t2-t1} seconds')

if __name__ == '__main__':
    run()
