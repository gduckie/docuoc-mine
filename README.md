# DocuOC using Yolov4 Darknet

## Prerequisites

- Docker installed
- If you have NVIDIA graphic card, following this installation to install NVIDIA container runtime to run this project by GPU ([Installation Guide](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html))

## Installation

CPU:

```
docker build . -t docuoc-cpu-v1-docker -f Dockerfile-cpu
```

GPU

```
docker build . -t docuoc-gpu-v1-docker -f Dockerfile-gpu
```

## Command

Detect single image:

- GPU

```sh
$ docker run --runtime=nvidia --rm -v $PWD:/workspace -w /workspace docuoc-gpu-v1-docker \
        darknet detector test data/obj.data cfg/yolov4-obj.cfg yolov4-obj_last.weights -i 0 -thresh 0.5 <image-path> -ext_output
```

- CPU

```sh
$ docker run --rm -v $PWD:/workspace -w /workspace docuoc-cpu-v1-docker \
        darknet detector test data/obj.data cfg/yolov4-obj.cfg yolov4-obj_last.weights -i 0 -thresh 0.5 <image-path> -ext_output
```

Training:
Note: If you have new data, run from step 1. If you want to test, I already put some data in `obj` and `test` folder for testing, just run step `4`.

1. Copy data:

- Copy all your train data to `data/obj` folder
- Copy all your test data to `data/test` folder

3. In root folder, generate location file:

- Run `python3 generate_test.py` to generate test file direction
- Run `python3 generate_train.py` to generate train file direction

4. Run training:

- GPU

```sh
$ docker run --runtime=nvidia --rm -v $PWD:/workspace -w /workspace docuoc-<cpu | gpu>-v1-docker \
        darknet detector train data/obj.data cfg/yolov4-obj.cfg yolov4.conv.137 -dont_show -map
```

- CPU

```sh
$ docker run --rm -v $PWD:/workspace -w /workspace docuoc-<cpu | gpu>-v1-docker \
        darknet detector train data/obj.data cfg/yolov4-obj.cfg yolov4.conv.137 -dont_show -map
```

## New script:

### 1. Extract data from images:

Extract data from images (Currently, it just working with Business Registration Certificate).

```sh
usage: extract.py [-h] -s SOURCE -d DESTINATION [-c CONFIG] [-dt DATA]
                  [-w WEIGHT] [-e EXTEND]

optional arguments:
  -h, --help            show this help message and exit
  -s SOURCE, --source SOURCE
                        path to image file
  -d DESTINATION, --destination DESTINATION
                        path to destination of result
  -c CONFIG, --config CONFIG
                        path to config file (optional). If not set, default is
                        'cfg/yolov4-obj.cfg'
  -dt DATA, --data DATA
                        path to data file (optional). If not set, default is
                        'data/obj.data'
  -w WEIGHT, --weight WEIGHT
                        path to weight file (optional). If not set, default is
                        'yolov4-obj_last.weights'
  -e EXTEND, --extend EXTEND
                        list value for extending detected boxes. Type: xmin,
                        ymin, xmax, ymax, Default: 0, 0, 0, 0'
```

Example:

```sh
docker run --rm -v $PWD:/workspace -w /workspace docuoc-<cpu | gpu>-v1-docker \
        python3 extract.py -s image.jpg -d destination -e 40,10,20,10
```

NOTE: IMPROVING EXTRACTED RESULT (COMING SOON):

- Increase/Decrease bounding boxes
- Extend width

NEW UPDATE: Automatically updating cfg file based on `data/obj.names`, so remember to update that file before running this script.

### 2. Augmented data:

Purpose: Create more data

- Automatically adjust image
- Type: brightness (increase/decrease), saturation (increase/decrease), noise (add)

```sh
usage: augment_images.py [-h] -d DESTINATION -s SOURCE

optional arguments:
  -h, --help            show this help message and exit
  -d DESTINATION, --destination DESTINATION
                        path to destination directory of images
  -s SOURCE, --source SOURCE
                        path to source directory of images
```

Example:

```sh
docker run --rm -v $PWD:/workspace -w /workspace docuoc-<cpu | gpu>-v1-docker \
        python3 augment_images.py -s source -d destination
```

### 2. Copy file:

- Automatically copy data from `source` dataset to `destination`
- Automatically split data into 80% in train and 20% test

Note: In training script, it also includes copy dataset to `data` folder. This script is just used if you want to copy by yourself.

```sh
usage: copy_files_train.py [-h] -d DESTINATION -s SOURCE

optional arguments:
  -h, --help            show this help message and exit
  -d DESTINATION, --destination DESTINATION
                        path to destination directory of images
  -s SOURCE, --source SOURCE
                        path to source directory of images
```

Example:

```sh
docker run --rm -v $PWD:/workspace -w /workspace docuoc-<cpu | gpu>-v1-docker \
       python3 copy_files_train.py -s ./train/obj -d ./train/train_0909
```

### 3. Training:

## Train default

- Automatically copy data from `dataset` to `data` folder
- Allow users to set max iterations
- Allow users to set weights saving folder
- Automatically generate train + test file
- Run training with config above

```sh
optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG, --config CONFIG (required)
                        path to config file
  -d DATASET, --dataset DATASET (required)
                        path to dataset folder
  -m MAX_ITERATIONS, --max_iterations MAX_ITERATIONS (optional)
                        set max-iterations you want to train. If not set, the
                        default value is 6000
  -w WEIGHTS_FOLDER, --weights_folder WEIGHTS_FOLDER (required)
                        path to weights folder you want to save
  -wl WEIGHTS_LAST, --weights_last WEIGHTS_LAST (optional)
                        path to last weights file that you want continuing
                        training from last weights
```

Example:

```sh
docker run --rm -v $PWD:/workspace -w /workspace docuoc-<cpu | gpu>-v1-docker \
        python3 train.py --config=cfg/yolov4-obj.cfg --dataset=dataset --max_iterations=6000 --weights_folder=data --weights_last=yolov4-obj_last.weights
```

## Train inner (NEW UPDATE)

Training with automatically updating:

- Amount of classes
- Amount of iterations
- Config
- Save edited config file in `weight folder` args

NOTE: Remember to add `data/obj.names` (classes), the script will edit config based on that file.

```sh
usage: train_inner_data.py [-h] [-c CONFIG] [-m MAX_ITERATIONS] -w WEIGHTS_FOLDER [-wl WEIGHTS_LAST] [-map]

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG, --config CONFIG (Optional)
                        path to config file
  -m MAX_ITERATIONS, --max_iterations MAX_ITERATIONS (Optional)
                        set max-iterations you want to train. If not set, the default value is 6000
  -w WEIGHTS_FOLDER, --weights_folder WEIGHTS_FOLDER (Required)
                        path to weights folder you want to save
  -wl WEIGHTS_LAST, --weights_last WEIGHTS_LAST (Optional)
                        path to last weights file that you want continuing training from last weights
  -map                  Some helpful text that is not bar. Default = True (Optional)

```

Example:

```sh
docker run --rm --runtime=nvidia -v $PWD:/workspace -w /workspace docuoc-gpu-v1-docker \
        python3 train_inner_data.py -w save_weight -m 8000 -map

```

## Cut meaningful text by json

```sh
usage: cut_mean_text.py [-h] -i IMG -r RESULT [-d DESTINATION] [-e EXTEND]

optional arguments:
  -h, --help            show this help message and exit
  -i IMG, --img IMG     path to image file (required)
  -r RESULT, --result RESULT
                        path to result file json (required)
  -d DESTINATION, --destination DESTINATION
                        path to destination of cut images (required)
  -e EXTEND, --extend EXTEND
                        list value for extending detected boxes. Type: xmin, xmax, ymin, ymax, Default: 0, 0, 0, 0' (optional)
```
JSON Example:
```sh
{
  "results": [
    {
      "name": "Taxation Type Y/N",
      "box": {
        "x": "  72",
        "y": "2887",
        "w": "1689",
        "h": " 110"
      }
    },
  ...
  ]
}
```
## Deskew image
Deskew rotated images
```sh
usage: deskew_img.py [-h] -i IMG [-s SAVE]

optional arguments:
  -h, --help            show this help message and exit
  -i IMG, --img IMG     path to image file (required)
  -s SAVE, --save SAVE  path to save image (required) ex: destination/save.png
```

## Rotate image
Rotate image.

NOTE: Rotated retry `rotated_retry = [90, 180, -90, -180]`

```sh
usage: rotate_img.py [-h] -i IMG -d DEGREE [-s SAVE]

optional arguments:
  -h, --help            show this help message and exit
  -i IMG, --img IMG     path to image file (required)
  -d DEGREE, --degree DEGREE (required)
                        degree for rotating
  -s SAVE, --save SAVE  path to save image (required) ex: destination/save.png
```
