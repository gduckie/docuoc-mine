import os
import shutil
import argparse
from var import image_valid
import time

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-s", "--source", required=True,
    help="path to source directory of images")
ap.add_argument("-t", "--type", required=True,
    help="type check")
args = vars(ap.parse_args())

source = args["source"]
type = args["type"]


def main_process():
    t1 = time.perf_counter()
    count = 0
    global type
    if type == 'images':
        check_image = []
        for _, file_name in enumerate(os.listdir(source)):
            full_file_name = os.path.join(source, file_name)
            if os.path.isfile(full_file_name) and 'txt' not in file_name and not os.path.isfile(os.path.join(source, os.path.splitext(file_name)[0] + '.txt')):
                print(full_file_name)
                check_image.append(full_file_name)
                count+=1
        print('{} images has no annotation'.format(str(len(check_image))))
    if type == 'txt':
        check_txt = []
        for _, file_name in enumerate(os.listdir(source)):
            full_file_name = os.path.join(source, file_name)

            if os.path.isfile(full_file_name) and 'txt' in file_name:
                for type in image_valid:
                    is_valid = False
                    if os.path.isfile(os.path.join(source, os.path.splitext(file_name)[0] + '.' + type)):
                        is_valid = True
                        break
                if is_valid == False:
                    check_txt.append(full_file_name)
                # print(full_file_name)
                count+=1
        print('{} txt has no images'.format(str(len(check_txt))))
        print(check_txt)

    t2 = time.perf_counter()
    print(f'Finished in {t2-t1} seconds')

def main():
    main_process()

if __name__ == '__main__':

    # Calling main() function
    main()
