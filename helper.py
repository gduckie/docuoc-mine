increase_height = 30
increase_width = 100
import shutil
from PIL import Image
import cv2

def yolobbox2bbox(x,y,w,h, extend):
    ext = extend.split(',')
    xmin = x
    ymin = y
    xmax = w + xmin
    ymax = h + ymin
    print((xmin - int(ext[0]), ymin - int(ext[1]), xmax + int(ext[2]), ymax + int(ext[3])))
    return xmin - int(ext[0]), ymin - int(ext[1]), xmax + int(ext[2]), ymax + int(ext[3])

def cut_at(img, coor):
    # image = Image.open(img)
    # cropped = image.crop(coor)
    # cropped.save("{}.png".format('test_cropped'), 'PNG')
    x = int(coor[0])
    y = int(coor[1])
    w = int(coor[2])
    h = int(coor[3])

    image = cv2.imread(img)
    crop = image[y:y+h, x:x+w]

    cv2.imwrite("test_cropped.jpg", crop)

def get_info(path):
    font     = cv2.FONT_HERSHEY_SIMPLEX
    fontScale = 0.5
    fontColor  = (255,0,0)
    lineType = 1

    #Threshold
    image = cv2.imread(path)

    height,width,channel = image.shape

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    T = threshold_local(gray, 15, offset = 6, method = "gaussian") # generic, mean, median, gaussian
    thresh = (gray > T).astype("uint8") * 255
    thresh = ~thresh

    #Dilation
    kernel =np.ones((1,1), np.uint8)
    ero = cv2.erode(thresh, kernel, iterations= 1)
    img_dilation = cv2.dilate(ero, kernel, iterations=1)

    # # Remove noise
    # nlabels, labels, stats, centroids = cv2.connectedComponentsWithStats(img_dilation, None, None, None, 8, cv2.CV_32S)
    # sizes = stats[1:, -1] #get CC_STAT_AREA component
    # final = np.zeros((labels.shape), np.uint8)
    # for i in range(0, nlabels - 1):
    #     if sizes[i] >= 10:   #filter small dotted regions
    #         final[labels == i + 1] = 255
    return img_dilation

def is_not_blank(s):
    return bool(s and not s.isspace())

def edit_file_helper(path, item_to_replace, value):
    count = 0
    line_num_to_edit = 0
    read_config = open(path, "r")
    lines = read_config.readlines()

    write_config = open(path, "w")
    result = lines
    for line in lines:
        count += 1
        if item_to_replace in line.strip():
            line_num_to_edit = count - 1
            if(line_num_to_edit == 0):
                result[count - 1] = value
            else:
                result[line_num_to_edit] = value


    write_config.writelines(result)
    write_config.close()
    # print("Line{}: {}".format(count, line.strip()))
    print("Edit: " + item_to_replace)
    print("Done edit file" + path)

def edit_file_cfg_helper(path, line_numbers, value): # line_number: Array[]
    num_line_edited = 0
    read_config = open(path, "r")
    lines = read_config.readlines()

    write_config = open(path, "w")
    result = lines

    for count, line_number in enumerate(line_numbers):
        line_num_to_edit = line_number - 1
        result[line_num_to_edit] = value
        num_line_edited += 1
    write_config.writelines(result)
    write_config.close()
    # print("Line{}: {}".format(count, line.strip()))

    print("Edited: " + str(num_line_edited))
    print("Done edit file " + path)

def validate_classes(path_obj_names, max_iterations, weight_folder):
    config_path = 'cfg/yolov4-obj.cfg'
    obj_data_path = 'data/obj.data'
    classes_amount = 0

    # get num classes from path_names
    read_config = open(path_obj_names, "r")
    lines = read_config.readlines()


    try:
        # get num classes from path_names
        read_config = open(path_obj_names, "r")
        lines = read_config.readlines()
        #
        for line in lines:
            if is_not_blank(line.strip()):
                classes_amount += 1
        if classes_amount == 0:
            print("there are no classes found! Please check again.")
            return False
        print(classes_amount)
        # edit file config
            # edit classes_amount
        edit_file_helper(config_path, "classes", "classes = {}\n".format(classes_amount))
            #edit filters
        cfg_filters_line_edit = [603, 689, 776, 866, 955]
        val_replace = "filters={}\n".format(str((classes_amount + 5)*3))
        edit_file_cfg_helper(config_path, cfg_filters_line_edit, val_replace)

            # edit max_iterations
        if max_iterations != '':
            edit_file_helper(config_path, "max_batches", "max_batches = {}\n".format(max_iterations))
            edit_file_helper(config_path, "steps=", "steps={},{}\n".format(int(int(max_iterations)*0.8), int(int(max_iterations)*0.9)))
        # edit file data
        edit_file_helper(obj_data_path, "classes", "classes = {}\n".format(classes_amount))
        # copy cfg to weight folder saving
        if weight_folder != '':
            shutil.copy(config_path, '{}/yolov4-obj.cfg'.format(weight_folder))
        return True
    except:
        print(" An exception occurred")
        return False
def log_info(processed, total):
    print("json{" + "\"processed\": \"{}\", \"total\": \"{}\"".format(processed, total) +"}json")

def log(message_type, message):
    if message_type == "error":
        print("error{" + "\"type\": \"Error\", \"message\": \"{}\"".format(message) + "}")
    else:
        print("{" + "{type: 'Success', message: '{}'}".format(message) + "}")
