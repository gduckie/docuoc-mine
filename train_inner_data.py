import subprocess
import argparse
import os
from helper import validate_classes


# Ex:
# 1. python3 train.py --config=cfg/yolov4-obj.cfg --dataset=dataset --max_iterations=6000 --weights_folder=data --weights_last=yolov4-obj_last.weights
# 2 .python3 train.py -c cfg/yolov4-obj.cfg -d dataset -m 5000 -w data/backup -wl data/backup/yolov4-obj_last.weights
#Read arguments
obj_data_path = "data/obj.data"

ap = argparse.ArgumentParser()
# config
ap.add_argument("-c", "--config", required=False,
    help="path to config file")
# max iterations
ap.add_argument("-m", "--max_iterations", required=False,
    help="set max-iterations you want to train. If not set, the default value is 6000")
# weights file folder
ap.add_argument("-w", "--weights_folder", required=True,
    help="path to weights folder you want to save")
# last weights file
ap.add_argument("-wl", "--weights_last", required=False,
    help="path to last weights file that you want continuing training from last weights")
# last weights file
ap.add_argument('-map', default=False, action='store_true', help='Some helpful text that is not bar. Default = True')
# ap.add_argument("-map", "--enable_map", required=False,
#     help="Bool: True/False, enable calculating map while training")
# last weights file
# ap.add_argument("-cn", "--container_name", required=True,
#     help="choose container you want to run detection or training")


args = vars(ap.parse_args())


config_file = args["config"] if args["config"] is not None else "cfg/yolov4-obj.cfg"
max_iterations = args["max_iterations"] if args["max_iterations"] is not None else "6000"
weights_folder = args["weights_folder"]
weights_last = args["weights_last"]
enable_map = args["map"]
# container_name = args["container_name"]

detect = '''docker run --rm -v $PWD:/workspace -w /workspace darknet-cpu-v1-docker darknet detector test data/obj.data cfg/yolov4-obj.cfg yolov4-obj_last.weights -i 0 -thresh 0.6 data/dog.jpg -ext_output '''

type_enum = {
    "error": 0,
    "success": 1
}
def log(message_type, message):
    if message_type == type_enum["error"]:
        print("{" + "type: 'Error', message: '{}'".format(message) + "}")
    else:
        print("{" + "{type: 'Success', message: '{}'}".format(message) + "}")

# Validating
def validate():
    # global max_iterations
    # validate config file
    if not os.path.isfile(config_file):
        log(type_enum["error"], "Config file is not existed")
        return False
    # validate weights saving folder
    if weights_folder is not None and not os.path.isdir(weights_folder):
        log(type_enum["error"], "Weights folder is not existed")
        return False
    # validate
    # if max_iterations is not None:
    #     try:
    #         max_iterations = int(max_iterations)
    #     except ValueError:
    #         log(type_enum["error"], "Max iterations is invalid")
    #         return False
    # else:
    #     max_iterations = 6000
    # validate weights_last file
    if weights_last and not os.path.isfile(weights_last):
        log(type_enum["error"], "Last weights file is not existed")
        return False
    if not validate_classes('data/obj.names', max_iterations, weights_folder):
        log(type_enum["error"], "Error in validating classes, please check again!")
        return False
    return True

def edit_file_helper(path, item_to_replace, value):
    count = 0
    line_num_to_edit = 0
    read_config = open(path, "r")
    lines = read_config.readlines()

    write_config = open(path, "w")

    for line in lines:
        count += 1
        if item_to_replace in line.strip():
            line_num_to_edit = count - 1
            break

    if(line_num_to_edit == 0):
        lines[count - 1] = value
    else:
        lines[line_num_to_edit] = value
    write_config.writelines(lines)
    write_config.close()
    # print("Line{}: {}".format(count, line.strip()))
    print("Done edit file" + path)



# Set max iteration and Set folder saving weight file
def edit_config_file():
    # # Edit config file
    # if max_iterations is not None: edit_file_helper(config_file, "max_batches", "max_batches = {}\n".format(max_iterations))
    # Edit place to save weights file
    if weights_folder is not None: edit_file_helper(obj_data_path, "backup", "backup = {}\n".format(weights_folder))

def run_shell(cmd):
    subprocess.run(cmd, shell=True)




#run train
def train():
    # train = '''docker run --rm -v $PWD:/workspace -w /workspace ''' + container_name + ''' darknet detector train data/obj.data ''' + config_file  + ''' yolov4.conv.137 -dont_show -map'''
    # generate_train = '''docker run --rm -v $PWD:/workspace -w /workspace ''' + container_name + ''' python3 generate_train.py'''
    # generate_test = '''docker run --rm -v $PWD:/workspace -w /workspace ''' + container_name + ''' python3 generate_test.py'''
    map = '-map' if enable_map else ''
    train_new = '''darknet detector train data/obj.data ''' + config_file  + ''' yolov4.conv.137 -dont_show '''  + map
    generate_train = '''python3 generate_train.py'''
    generate_test = '''python3 generate_test.py'''

    is_validate = validate()
    if not is_validate: return
    edit_config_file()
    # generate train + text
    run_shell(generate_train)
    run_shell(generate_test)
    # run train
    # print(train_continue if weights_last is not None else train_new)

    if weights_last is not None:
        train_continue = '''darknet detector train data/obj.data ''' + config_file + ' ' + weights_last + ''' -dont_show ''' + map
        print('run: ' + train_continue)
        run_shell(train_continue)
    else:
        print('run: ' + train_new)
        run_shell(train_new)
    # subprocess.run(shell, shell=True)


if __name__ == '__main__':

    # Calling main() function
    train()
