import os
import shutil
import argparse
import subprocess

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--destination", required=True,
    help="path to destination directory of images")
ap.add_argument("-s", "--source", required=True,
    help="path to source directory of images")
ap.add_argument("-a", "--amount", required=False,
    help="number of files to copy")
args = vars(ap.parse_args())

source = args["source"]
dest = args["destination"]
amount = args["amount"]

imageTail = ['png', 'jpg', 'gif', 'jpeg']
img_len = len([name for name in os.listdir(source) if os.path.isfile(os.path.join(source, name)) and name.split(".")[-1] in imageTail])
txt_len = len([name for name in os.listdir(source) if os.path.isfile(os.path.join(source, name)) and name.endswith('txt')])

number_test_file = round(img_len*0.2)
number_train_file = img_len - number_test_file

def run_shell(cmd):
    subprocess.run(cmd, shell=True)
def validate_folder():
    if not os.path.isdir(dest + '/test'):
        os.mkdir(dest + '/test')
    if not os.path.isdir(dest + '/obj'):
        os.mkdir(dest + '/obj')



def copy(source, destination):
    count = 0
    for _, file_name in enumerate(os.listdir(source)):
        if amount is not None and count == int(amount):
            print('break')
            break
        full_file_name = os.path.join(source, file_name)
        if 'txt' not in file_name:
          run_shell('mv -f "{}"* "{}"'.format(full_file_name.split(".")[0], dest))
          count+=1
          # print('move: ' + full_file_name)
          # shutil.move(full_file_name, os.path.join(destination, file_name))



def main_process():
    copy(source, dest)



def main():
    main_process()

if __name__ == '__main__':

    # Calling main() function
    main()
