import argparse
from PIL import Image
from var import image_valid
from helper import yolobbox2bbox
import time
import json
import cv2

ap = argparse.ArgumentParser()
# path to test folder
ap.add_argument("-i", "--img", required=True,
    help="path to image file")
ap.add_argument("-r", "--result", required=True,
    help="path to result file txt")
ap.add_argument("-d", "--destination", required=True,
    help="path to destination of result")
ap.add_argument("-e", "--extend", required=False,
    help="list value for extending detected boxes. Type: xmin, xmax, ymin, ymax, Default: 0, 0, 0, 0'")

args = vars(ap.parse_args())

img_path = args["img"]
result_file_path = args["result"]
extend = args["extend"] if args["extend"] is not None else '0,0,0,0'
dest = args["destination"]

def run():
    read_result_file = open(result_file_path, "r")
    keyword_found = []
    t1 = time.perf_counter()

    read_data = json.loads(read_result_file.read())
    data = read_data['results']
    if not data:
        print('cannot read file extracted in json file')
        return
    # image = Image.open(img_path)
    # for result in data:
    #     class_name = result['name'].replace('/', '')
    #     x = int(result['box']['x'])
    #     y = int(result['box']['y'])
    #     w = int(result['box']['w'])
    #     h = int(result['box']['h'])
    #     cropped = image.crop(yolobbox2bbox(x, y, w, h, extend))
    #     cropped.save("{}/{}.png".format(dest, class_name), 'PNG')
    #     keyword_found.append(class_name)
    image = cv2.imread(img_path)
    count_duplicate = 0
    for result in data:
        class_name = result['name'].replace('/', '')
        x = abs(int(result['box']['x']))
        y = abs(int(result['box']['y']))
        w = abs(int(result['box']['w']))
        h = abs(int(result['box']['h']))
        coor_extended = yolobbox2bbox(x, y, w, h, extend)
        cropped = image[coor_extended[1]:coor_extended[3], coor_extended[0]:coor_extended[2]]
        cv2.imwrite("{}/{}{}.png".format(dest, class_name, "_{}".format(count_duplicate) if class_name in keyword_found else ""), cropped)
        if class_name in keyword_found:
            count_duplicate += 1
        keyword_found.append(class_name)

    print(f'FOUND: {len(keyword_found)} keywords')
    print("json{" + "\"keyword_found\": \"{}\"".format(keyword_found)  + "}json")
    t2 = time.perf_counter()
    print(f'Finished in {t2-t1} seconds')

if __name__ == '__main__':
    run()
