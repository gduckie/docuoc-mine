import argparse
import cv2
from de import deskew

ap = argparse.ArgumentParser()
# path to test folder
ap.add_argument("-i", "--img", required=True,
    help="path to image file")
ap.add_argument("-s", "--save", required=True,
    help="path to save image")

args = vars(ap.parse_args())
img_path = args["img"]
save_img = args["save"]


def main():
    img_deskew = deskew(img_path)
    cv2.imwrite(save_img, img_deskew)

if __name__ == '__main__':
    main()
