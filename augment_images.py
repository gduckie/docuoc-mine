from PIL import ImageEnhance
from PIL import Image
from PIL import ImageFilter
import os
import cv2
import random
import numpy as np
import argparse
from var import image_valid
import time
import concurrent.futures
import subprocess
import shutil
from var import image_valid
from helper import log
from helper import log_info
from augment_bg import execute_main as augment_bg
from augment_bg import len_bg

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--destination", required=True,
    help="path to destination directory of images")
ap.add_argument("-s", "--source", required=True,
    help="path to source directory of images")
args = vars(ap.parse_args())

source = args["source"]
dest = args["destination"]


low = 0.5
high = 3
noise_val = 0.07



def copy(source, destination):
    shutil.copy(source, destination)

def change_brightness(image_url, value): # 0.5 dark, 3 bright
    img = Image.open(image_url)
    #increasing the brightness 20%
    new_image = ImageEnhance.Brightness(img).enhance(value)
    value_rate = 'low' if value < 1 else 'high'
    new_image.save('{}/{}_{}_brightness.{}'.format(dest, os.path.splitext(image_url)[0].split('/')[-1], value_rate, image_url.split(".")[-1]))
    copy('{}.txt'.format(os.path.splitext(image_url)[0]), '{}/{}_{}_brightness.txt'.format(dest, os.path.splitext(image_url)[0].split('/')[-1], value_rate))




def change_contrast(image_url, value): # 0.5 blur, 3 contrast
    img = Image.open(image_url)
    #increasing the contrast 20%
    new_image = ImageEnhance.Contrast(img).enhance(value)
    value_rate = 'low' if value < 1 else 'high'
    new_image.save('{}/{}_{}_contrast.{}'.format(dest, os.path.splitext(image_url)[0].split('/')[-1], value_rate, image_url.split(".")[-1]))
    copy('{}.txt'.format(os.path.splitext(image_url)[0]), '{}/{}_{}_contrast.txt'.format(dest, os.path.splitext(image_url)[0].split('/')[-1], value_rate))

def change_saturation(image_url, value): # 0.5 blur, 3 contrast
    img = Image.open(image_url)
    #increasing the contrast 20%
    new_image = ImageEnhance.Color(img).enhance(value)
    value_rate = 'low' if value < 1 else 'high'
    new_image.save('{}/{}_{}_saturation.{}'.format(dest, os.path.splitext(image_url)[0].split('/')[-1], value_rate, image_url.split(".")[-1]))
    copy('{}.txt'.format(os.path.splitext(image_url)[0]), '{}/{}_{}_saturation.txt'.format(dest, os.path.splitext(image_url)[0].split('/')[-1], value_rate))

def sp_noise(image_url,prob): # 0.07
    image = cv2.imread(image_url)
    output = np.zeros(image.shape,np.uint8)
    thres = 1 - prob
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            rdn = random.random()
            if rdn < prob:
                output[i][j] = 0
            elif rdn > thres:
                output[i][j] = 255
            else:
                output[i][j] = image[i][j]
    cv2.imwrite('{}/{}_noise.{}'.format(dest, os.path.splitext(image_url)[0].split('/')[-1], image_url.split(".")[-1]), output)
    copy('{}.txt'.format(os.path.splitext(image_url)[0]), '{}/{}_noise.txt'.format(dest, os.path.splitext(image_url)[0].split('/')[-1]))

def blur(image_url, value):
    img = Image.open(image_url)

    img_blured = img.filter(ImageFilter.GaussianBlur(radius = value))

    img_blured.save('{}/{}_{}_blur.{}'.format(dest, os.path.splitext(image_url)[0].split('/')[-1], value, image_url.split(".")[-1]))
    copy('{}.txt'.format(os.path.splitext(image_url)[0]), '{}/{}_{}_blur.txt'.format(dest, os.path.splitext(image_url)[0].split('/')[-1], value))

def increase_color(image_path, color):
    im = Image.open(image_path).convert('RGB')

    r, g, b = im.split()

    if color == 'green':
        r = r.point(lambda i: i * 0.9)
        g = g.point(lambda i: i * 1.2)
    else:
        r = r.point(lambda i: i * 1.2)
        g = g.point(lambda i: i * 0.9)

    result = Image.merge('RGB', (r, g, b))
    result.save('{}/{}_{}.{}'.format(dest, os.path.splitext(image_path)[0].split('/')[-1], color, image_path.split(".")[-1]))
    copy('{}.txt'.format(os.path.splitext(image_path)[0]), '{}/{}_{}.txt'.format(dest, os.path.splitext(image_path)[0].split('/')[-1], color))

def helper(filename_path):
    # # adjust brightness
    change_brightness(filename_path, 0.25)

    change_contrast(filename_path, 0.25)


    # adjust saturation
    # change_saturation(filename_path, 0.5)
    # change_saturation(filename_path, 3)

    # adjust noise
    sp_noise(filename_path, 0.03)

    #increase red
    increase_color(filename_path, 'red')
    increase_color(filename_path, 'green')
    

processed = 0
total_adjust = 7

def copy_files(source, dest):
    num_copied = 0
    print("copy original files to destination")
    for count, file_name in enumerate(os.listdir(source)):
        full_file_name = os.path.join(source, file_name)
        shutil.copy(full_file_name, os.path.join( dest, file_name))
        num_copied += 1

def copy_file(source_file, dest_file, count):
    total_file_source = len([name for name in os.listdir(source)])

    log_info(count, total_file_source)
    shutil.copy(source_file, dest_file)

def helper_copy(para):
    copy_file(para[0], para[1], para[2])

def copy_original_files():
    print("Start copying original files to destination")
    para = []
    count = 1
    for _, filename in enumerate(os.listdir(source)):
        if os.path.isfile(os.path.join(source, filename)):
            para.append((os.path.join(source, filename), os.path.join( dest, filename), count))
            count+=1
    with concurrent.futures.ProcessPoolExecutor() as executor:
        executor.map(helper_copy, para)

def main_process():
    t1 = time.perf_counter()
    para = []
    count = 1
    
    
    total_bg = len_bg()
    
    
    img_len_original = len([name for name in os.listdir(source) if os.path.isfile(os.path.join(source, name)) and name.split(".")[-1] in image_valid])
    total_bg_augment = img_len_original * total_bg
    total = str((img_len_original + total_bg_augment) * total_adjust)
    global processed

    # augment background
    augment_bg(source, source)

    # copy all original data to destination
    copy_original_files()

    # augment images
    for count, file_name in enumerate(os.listdir(source)):
        full_file_name = os.path.join(source, file_name)
        file_type = file_name.split(".")[-1]
        if os.path.isfile(full_file_name) and 'txt' not in file_name and os.path.isfile(os.path.join(source, os.path.splitext(file_name)[0] + '.txt')):
            para.append((full_file_name))
            count+=1

    img_len = len([name for name in os.listdir(source) if os.path.isfile(os.path.join(source, name)) and name.split(".")[-1] in image_valid])
    txt_len = len([name for name in os.listdir(source) if os.path.isfile(os.path.join(source, name)) and name.endswith('txt')])
    if(img_len != txt_len):
        log("error", "number of images and text files are invalid. Please make it equal!")
        return
    
    
    with concurrent.futures.ProcessPoolExecutor() as executor:
        for logs in executor.map(helper, para):
            processed += total_adjust
            print("\n==========================================================\n")
            log_info(processed, total)
            print("\n==========================================================\n")

    t2 = time.perf_counter()
    print(f'Finished in {t2-t1} seconds')

def main():
    main_process()

if __name__ == '__main__':

    # Calling main() function
    main()
