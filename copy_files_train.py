import os
import shutil
import argparse
import imghdr
from var import image_valid
import concurrent.futures
import time
import subprocess
# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--destination", required=True,
    help="path to destination directory of images")
ap.add_argument("-s", "--source", required=True,
    help="path to source directory of images")
args = vars(ap.parse_args())

source = args["source"]
dest = args["destination"]


img_len = len([name for name in os.listdir(source) if os.path.isfile(os.path.join(source, name)) and name.split(".")[-1] in image_valid])
txt_len = len([name for name in os.listdir(source) if os.path.isfile(os.path.join(source, name)) and name.endswith('txt')])

number_test_file = round(img_len*0.2)
number_train_file = img_len - number_test_file


def validate_folder():
    if not os.path.isdir(dest + '/test'):
        os.mkdir(dest + '/test')
    if not os.path.isdir(dest + '/obj'):
        os.mkdir(dest + '/obj')



def copy_old(source):
    num_copied = 0
    for count, file_name in enumerate(os.listdir(source)):
        full_file_name = os.path.join(source, file_name)
        file_type = file_name.split(".")[-1]
        if os.path.isfile(full_file_name) and 'txt' not in file_name and file_type in image_valid and os.path.isfile(os.path.join(source, os.path.splitext(file_name)[0] + '.txt')):
            num_copied += 1
            if num_copied <= number_train_file:
                shutil.copy(full_file_name, os.path.join( dest + '/obj', file_name))
                shutil.copy(os.path.join(source, os.path.splitext(file_name)[0] + '.txt'), os.path.join( dest + '/obj', os.path.splitext(file_name)[0] + '.txt'))
            else:
                shutil.copy(full_file_name, os.path.join(dest + '/test', file_name))
                shutil.copy(os.path.join(source, os.path.splitext(file_name)[0] + '.txt'), os.path.join(dest + '/test', os.path.splitext(file_name)[0] + '.txt'))

def helper_copy(para):
    copy(para[0], para[1])

def run_shell(cmd):
    subprocess.run(cmd, shell=True)

def copy(file_name, count):
    print('copy: "{}"'.format(file_name.split('.')[0]))
    full_file_name = os.path.join(source, file_name)
    file_type = file_name.split(".")[-1]
    if os.path.isfile(full_file_name):
        if count <= number_train_file:
            run_shell('cp -f "{}"* "{}"'.format(full_file_name.split(".")[0], dest + '/obj'))
        else:
            run_shell('cp -f "{}"* "{}"'.format(full_file_name.split(".")[0], dest + '/test'))



def main_process():
    t1 = time.perf_counter()
    validate_folder()
    if(img_len != txt_len):
        print("number of images and text files are invalid. Please make it equal!")
        return
    para = []
    count = 1
    for _, filename in enumerate(os.listdir(source)):
        if os.path.isfile(os.path.join(source, filename)) and filename.endswith(".txt"):
            para.append((filename, count))
            count+=1
    with concurrent.futures.ProcessPoolExecutor() as executor:
        executor.map(helper_copy, para)

    t2 = time.perf_counter()
    print(f'Finished in {t2-t1} seconds')



def main():
    main_process()

if __name__ == '__main__':

    # Calling main() function
    main()
