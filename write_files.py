import argparse
import os

ap = argparse.ArgumentParser()
ap.add_argument("-s", "--source", required=True,
    help="path to source directory of images")
args = vars(ap.parse_args())

source = args["source"]


def main():
    with open("files.txt", 'w') as f:
        for count, file_name in enumerate(os.listdir(source)):
            full_file_name = os.path.join(source, file_name)
            print(file_name)
            f.write("%s\n" % file_name)
    
if __name__ == '__main__':

    # Calling main() function
    main()