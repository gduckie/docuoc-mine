# x_left = (x * bg_w) - (w / 2)
# y_left = (y * bg_h) - (h / 2)

import subprocess
import argparse
import os
import json
from PIL import Image
from var import image_valid
# {
#     processed: number,
#     total: number,
#     duration: number,
#     accuracy: number,
#     avgDuration: number,
#     avgAccuracy: number
# }
# accuracy = correct_boxes / true_boxes


# Ex:
# 1.
#Read arguments

ap = argparse.ArgumentParser()
# path to test folder
ap.add_argument("-s", "--source", required=True,
    help="path to data test folder")
ap.add_argument("-c", "--config", required=False,
    help="path to config file (optional). If not set, default is 'cfg/yolov4-obj.cfg'")
ap.add_argument("-w", "--weight", required=False,
    help="path to weight file (optional). If not set, default is 'yolov4-obj_last.weights'")
#
args = vars(ap.parse_args())
#
#
datatest_folder = args["source"]
config_file = args["config"] if args["config"] is not None else "cfg/yolov4-obj.cfg"
weight_file = args["weight"] if args["weight"] is not None else "yolov4-obj_last.weights"

# datatest_folder = 'test'
# config_file = 'cfg/yolov4-obj.cfg'
# weight_file = 'yolov4-obj_last.weights'


img_len = len([name for name in os.listdir(datatest_folder) if os.path.isfile(os.path.join(datatest_folder, name)) and name.split(".")[-1] in image_valid])
txt_len = len([name for name in os.listdir(datatest_folder) if os.path.isfile(os.path.join(datatest_folder, name)) and name.endswith('txt')])






# calculate accuracy
# accuracy = total_correct_predicted / total_true_boxes

def log(processed, total, duration, accuracy, avgDuration, avgAccuracy):
    print("json{" + "\"processed\": \"{}\", \"total\": \"{}\", \"duration\": \"{}\", \"accuracy\": \"{}\", \"avgDuration\": \"{}\", \"avgAccuracy\": \"{}\"".format(processed, total, duration, accuracy, avgDuration, avgAccuracy) +"}")
def run_shell(cmd):
    t = subprocess.run(cmd, capture_output=True, text=True, shell=True).stdout.strip("\n")
    return t.split("\n")

test_data = ['3rdparty', 'add_background.py', 'predicted_time: 3245.0000', 'background_img', 'Blueprint: 100%', 'result{"x": "1698", "y": "1214", "w": "694", "h": "426"}']

def get_image_path(name):
    tail = ['png', 'jpg', 'gif', 'jpeg']
    for ext in tail:
        if os.path.isfile("{}/{}.{}".format(datatest_folder, name, ext)):
            return "{}/{}.{}".format(datatest_folder, name, ext)


def extract_result(img_path):
    detect = '''darknet detector test data/obj.data ''' + config_file + ' '  + weight_file + ''' -i 0 -thresh 0.6 ''' + img_path + ''' -dont_show'''
    result_detected = run_shell(detect)
    result_extracted = [rs.replace('result','') for rs in result_detected if 'result' in rs]

    predicted_time_extracted = [pt.replace('predicted_time: ','') for pt in result_detected if 'predicted_time' in pt]

    confidence_extracted = [c.replace('Blueprint: ','') for c in result_detected if 'Blueprint' in c]

    return result_extracted, predicted_time_extracted, confidence_extracted

# darknet detector test data/obj.data cfg/yolov4-obj.cfg yolov4-obj_last.weights -i 0 -thresh 0.6 data/test/black_bg_3d_equip_33.png -ext_output -dont_show
def run():
    # validate
    if(img_len != txt_len):
        print("number of images and text files are invalid. Please make it equal!")
        return

    processed = 0 # total processed images

    total = str(len(os.listdir(datatest_folder))/2)
    all_confidence = []
    all_duration = []
    all_accuracy = []
    avg_accuracy = 0
    avg_duration = 0
    accuracy = 0

    for count, file_name in enumerate(os.listdir(datatest_folder)):
        total_true_boxes = 0
        total_correct_predicted = 0
        total_predicted = 0

        if file_name.endswith('txt'):
            print('read file: ' + file_name)
            processed+=1
            labeled_file = file_name
            # 1. read labeled data
            read_labeled_file = open("{}/{}".format(datatest_folder, labeled_file), "r")
            lines_lableled = read_labeled_file.readlines()
            # 2. detecting with image path



            img_path = get_image_path(os.path.splitext(file_name)[0])

            result_extracted, predicted_time_extracted, confidence_extracted = extract_result(img_path)
            predicted_element = predicted_time_extracted[0] if len(predicted_time_extracted) > 0 else 0
            all_duration.append(float(predicted_element))

            confidence_element = 0


            avg_duration = float(sum(all_duration) / len(all_duration))
            # print(result)
            # print(predicted_time)
            # 3. Read deteting dataset
            total_predicted = len(result_extracted)
            if len(result_extracted) > 0 and len(lines_lableled) > 0:
                image_info = Image.open(img_path)
                width, height = image_info.size
                count_detected = 0
                for detected in result_extracted:
                    # extracted predicted and confidence
                    confidence_element = confidence_extracted[count_detected]

                    # print('sum duration: ' + str(avg_duration))
                    # print('sum all_confidence: ' + str(avg_confidence))
                    #
                    # print('predicted_element: ' + str(predicted_element))
                    # print('confidence_element: ' + str(confidence_element))
                    # extract detected data
                    data = json.loads(detected)
                    detect_x = float(data["x"])
                    detect_y = float(data["y"])
                    # extract labeled data
                    for labeled in lines_lableled:
                        labeled_data = labeled.split(' ')
                        labeled_w = float(labeled_data[3]) * width
                        labeled_h = float(labeled_data[4].replace('\n', '')) * height
                        labeled_x = float(labeled_data[1]) * width - labeled_w / 2
                        labeled_y = float(labeled_data[2]) * height - labeled_h / 2
# 4. compare data
                        if abs(labeled_x - detect_x)  < 200 and abs(labeled_y - detect_y) < 200:
                            total_correct_predicted += 1
                            break
    # 5. log json
                    count_detected += 1

            # elif len(result_extracted) == len(lines_lableled):
            #     print("len(result_extracted) == len(lines_lableled)")
            #     accuracy = float(100)
            #     all_accuracy.append(accuracy)
            #     avg_accuracy += float(accuracy / len(all_accuracy)) if len(all_accuracy) > 0 and accuracy > 0 else avg_accuracy

            print('total_correct_predicted: ' + str(total_correct_predicted))

            total_true_boxes = len(lines_lableled)
            print('total_true_boxes: ' + str(total_true_boxes))
            print('total_predicted: ' + str(total_predicted))
            accuracy_current = float(total_correct_predicted / total_true_boxes) if total_true_boxes > 0 else (1 if total_predicted == total_true_boxes else 0)
            print('accuracy_current: ' + str(accuracy_current * 100))
            accuracy_current = 1 if accuracy_current > 1 else accuracy_current
            all_accuracy.append(float(accuracy_current * 100))
            avg_accuracy = float(sum(all_accuracy) / len(all_accuracy)) if len(all_accuracy) > 0 else avg_accuracy

            log(str(processed), str(total), str(predicted_element), str(round(accuracy_current*100, 2)), str(round(avg_duration, 6)), str(round(avg_accuracy, 6)))
            print('\n=========================================================================================================\n')






if __name__ == '__main__':
    run()
    # d = '''docker run --rm -v $PWD:/workspace -w /workspace darknet-cpu-v1-docker darknet detector test data/obj.data cfg/yolov4-obj.cfg yolov4-obj_last.weights -i 0 -thresh 0.6 test/black_bg_3d_equip_32.png -dont_show'''
    # run_shell(d)
    # run testing
    # run()
