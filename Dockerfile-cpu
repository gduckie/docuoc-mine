FROM ubuntu:20.04 AS builder

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
      && apt-get install --no-install-recommends --no-install-suggests -y gnupg2 ca-certificates \
            git build-essential libopencv-dev \
      && rm -rf /var/lib/apt/lists/*

COPY configure.sh /tmp/

ARG CONFIG=cpu-cv

RUN chmod 755 /tmp/configure.sh

COPY . /darknet

RUN   cd darknet \
      && /tmp/configure.sh $CONFIG && make \
      && cp darknet /usr/local/bin

FROM ubuntu:20.04

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
      && apt-get install -y python3-pip git \
      && apt-get install -y python3-opencv \
      && pip3 install Pillow \
      && pip3 install git+git://github.com/sbrunner/deskew \
      && apt-get install -y software-properties-common \
      && add-apt-repository ppa:deadsnakes/ppa \
      && apt-get install -y python3.6 \
      && apt-get install --no-install-recommends --no-install-suggests -y libopencv-highgui4.2 \
      && rm -rf /var/lib/apt/lists/*

COPY --from=builder /usr/local/bin/darknet /usr/local/bin/darknet
COPY --from=builder /darknet /workspace
