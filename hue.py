from PIL import Image
im = Image.open('deskew_test.jpg').convert('RGB')

# Split into 3 channels
r, g, b = im.split()

# Increase Reds
# r = r.point(lambda i: i * 1.2)
r = r.point(lambda i: i * 0.9)

# Decrease Greens
# g = g.point(lambda i: i * 0.9)
g = g.point(lambda i: i * 1.2)

# Recombine back to RGB image
result = Image.merge('RGB', (r, g, b))

result.save('result.jpg')