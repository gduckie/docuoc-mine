import subprocess
import argparse
import os
from PIL import Image
from random import randint
from var import image_valid
import concurrent.futures
import time

# Ex:
# 1. python3 add_background.py -s source -d destination
# 2. python3 add_background.py --source=source --destination=destination
#Read arguments

ap = argparse.ArgumentParser()
# source
ap.add_argument("-s", "--source", required=True,
    help="path to source file")
# destination
ap.add_argument("-d", "--destination", required=True,
    help="path to dataset folder")

args = vars(ap.parse_args())


source = args["source"]
destination = args["destination"]
processed = 0
bg = [{"source": './background_img/black_bg.png', "name": 'black_bg'}, {"source": './background_img/colorful_bg.png', "name": 'colorful_bg'}, {"source": './background_img/document_bg.png', "name": 'document_bg'}, {"source": './background_img/white_bg.png', "name": 'white_bg'}]



def add_bg(bg_source, bg_name, filename, source_folder, dest_folder):
    # print('add_bg')
    background = Image.open(bg_source).convert('RGBA')
    bg_w, bg_h = background.size

    if filename.split(".")[-1] in image_valid:

        img = Image.open(source_folder + '/' +  filename)
        # print('./' + source_folder + '/' +  filename)
        img_w, img_h = img.size
        if not os.path.isfile(source_folder + '/' +  filename):
            return
        # Make sure img_w < bg_w
        if img_w > bg_w or img_h > bg_h:
            # print('resize ' + source_folder + '/' +  filename)
            newsize = (1500, 1000)
            img = img.resize(newsize)
            img.save(source_folder + '/' +  filename)
            img_w = 1500
            img_h = 1000
            # print('Cannot write this:')
            # print('./' + source_folder + '/' +  filename)
            # continue

        random_offset_x = randint(0, bg_w - img_w) # bg_w - img_w: To make sure image does not be overflow on background
        random_offset_y = randint(0, bg_h - img_h) # bg_h - img_h: To make sure image does not be overflow on background


        offset = (random_offset_x, random_offset_y)
        # Overlay image on background with random offset
        background.paste(img, offset)
        # background.save(dest_folder + '/' + bg_name + os.path.splitext(filename)[0] + '.png')
        # print("Write: " + "{}/{}_{}.{}".format(dest_folder, bg_name, os.path.splitext(filename)[0], "png"))
        background.save("{}/{}_{}.{}".format(dest_folder, bg_name, os.path.splitext(filename)[0], "png"))

        read_labeled_file = open("{}/{}.txt".format(source_folder, filename.split(".")), "r")
        lines_lableled = read_labeled_file.readlines()

        # calculate xmin, ymin, xmax, ymax
        xmin = random_offset_x
        ymin = random_offset_y
        xmax = random_offset_x + img_w
        ymax = random_offset_y + img_h

        # calculate yolo format
        x_center = float((xmin + xmax)) / 2 / bg_w
        y_center = float((ymin + ymax)) / 2 / bg_h
        w = float((xmax - xmin)) / bg_w
        h = float((ymax - ymin)) / bg_h
        # Create train file (txt) and write it with above xmin, ymin, xmax, ymax
        print("%d %.6f %.6f %.6f %.6f\n" % (0, x_center, y_center, w, h))
        # with open("{}/{}_{}.{}".format(dest_folder, bg_name, os.path.splitext(filename)[0], 'txt'), 'w') as f:
        #     # print("Write: " + "{}/{}_{}.{}".format(dest_folder, bg_name, os.path.splitext(filename)[0], 'txt'))
        #     f.write("%d %.6f %.6f %.6f %.6f\n" % (0, x_center, y_center, w, h))
        # log_info(str(processed), total)
        # with open(dest_folder + '/' + bg_name + os.path.splitext(filename)[0] + '.txt', 'w') as f:
        #     f.write("%d %.6f %.6f %.6f %.6f\n" % (0, x_center, y_center, w, h))
        # file = open(dest_folder + bg_name + os.path.splitext(filename)[0] + '.txt', 'w+')
        background.close()
        img.close()

    return f"done {filename}"



def main():
    t1 = time.perf_counter()
    add_bg('bg/white.png', 'white', 'gsc_1.jpg', source, destination)
    t2 = time.perf_counter()
    print(f'Finished in {t2-t1} seconds')

if __name__ == '__main__':

    # Calling main() function
    main()
