import math
from typing import Tuple, Union
import cv2
import numpy as np
from deskew import determine_skew
# import imutils
import time
from PIL import Image
import math

# def rotate2(image, angle):
#     rotated = imutils.rotate(image, angle)
#     return rotated

def rotate(image, angle, background):
    old_width, old_height = image.shape[:2]
    angle_radian = math.radians(angle)
    width = (abs(np.sin(angle_radian) * old_height) + abs(np.cos(angle_radian) * old_width))
    height = (abs(np.sin(angle_radian) * old_width) + abs(np.cos(angle_radian) * old_height))

    image_center = tuple(np.array(image.shape[1::-1]) / 2)
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
    rot_mat[1, 2] += (width - old_width) / 2
    rot_mat[0, 2] += (height - old_height) / 2
    return cv2.warpAffine(image, rot_mat, (int(round(height)), int(round(width))), borderValue=background)

def largest_rotated_rect(w, h, angle):
    quadrant = int(math.floor(angle / (math.pi / 2))) & 3
    sign_alpha = angle if ((quadrant & 1) == 0) else math.pi - angle
    alpha = (sign_alpha % math.pi + math.pi) % math.pi

    bb_w = w * math.cos(alpha) + h * math.sin(alpha)
    bb_h = w * math.sin(alpha) + h * math.cos(alpha)

    gamma = math.atan2(bb_w, bb_w) if (w < h) else math.atan2(bb_w, bb_w)

    delta = math.pi - alpha - gamma

    length = h if (w < h) else w

    d = length * math.cos(alpha)
    a = d * math.sin(alpha) / math.sin(delta)

    y = a * math.cos(gamma)
    x = y * math.tan(gamma)

    return (
        bb_w - 2 * x,
        bb_h - 2 * y
    )

def crop_around_center(image, width, height):
    image_size = (image.shape[1], image.shape[0])
    image_center = (int(image_size[0] * 0.5), int(image_size[1] * 0.5))

    if(width > image_size[0]):
        width = image_size[0]

    if(height > image_size[1]):
        height = image_size[1]

    x1 = int(image_center[0] - width * 0.5)
    x2 = int(image_center[0] + width * 0.5)
    y1 = int(image_center[1] - height * 0.5)
    y2 = int(image_center[1] + height * 0.5)

    return image[y1:y2, x1:x2]

def deskew(img):
    image = cv2.imread(img)
    # image = cv2.imread('output.png')
    grayscale = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    angle = determine_skew(grayscale)

    image_height, image_width = image.shape[0:2]

    if not angle:
        return image

    largest_w, largest_h = largest_rotated_rect(
        image_width,
        image_height,
        math.radians(angle)
    )



    deskew_img = rotate(image, angle, (0, 0, 0))
    image_rotated_cropped = crop_around_center(
        deskew_img,
        largest_w + 100,
        largest_h + 100
    )

    return image_rotated_cropped

def rotate_by_degree_save(img, degree):
    image = Image.open(img)
    rotated = image.rotate(degree)
    rotated.save(img)
    return rotated

def rotate_by_degree(img, degree):
    image = Image.open(img)
    rotated = image.rotate(degree, expand=True)
    return rotated
