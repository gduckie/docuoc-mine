import argparse
import cv2
from de import rotate_by_degree

ap = argparse.ArgumentParser()
# path to test folder
ap.add_argument("-i", "--img", required=True,
    help="path to image file")
ap.add_argument("-d", "--degree", required=True,
    help="degree for rotating")
ap.add_argument("-s", "--save", required=True,
    help="path to save image")

args = vars(ap.parse_args())

img_path = args["img"]
save_img = args["save"]
degree = args["degree"]

def main():
    rotated_img = rotate_by_degree(img_path, int(degree))
    rotated_img.save(save_img)

if __name__ == '__main__':
    main()
